using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAMarafon.Models
{

 public class InstructorCourseAssignment
 {
 [Key]
 public int ID { get; set; }
 [Required(ErrorMessage = "Instructor ID is Required")]
 public int InstructorID { get; set; }
 [Required(ErrorMessage = "Course ID is Required")]
 public string CourseID { get; set; }
 }
}
