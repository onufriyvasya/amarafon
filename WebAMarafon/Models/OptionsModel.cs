using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
namespace WebAMarafon.Models
{
publicclassOptions
 {
 [Key]
publicint ID { get; set; }
 [Required(ErrorMessage = "Option is Required !")]
publicstring Option { get; set; }
 [Required(ErrorMessage = "Status is Required !")]
publicstring Status { get; set; }
 [Required(ErrorMessage = "Question ID is Required !")]
publicint QuestionID { get; set; }
 }
}