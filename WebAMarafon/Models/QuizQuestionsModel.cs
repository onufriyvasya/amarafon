using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAMarafon.Models
{
publicclassQuizQuestions
 {
 [Key]
publicint ID { get; set; }
 [Required(ErrorMessage = "Question ID is Required !")]
publicint QuestionID { get; set; }
 [Required(ErrorMessage = "Quiz ID is Required !")]
publicint QuizID { get; set; }
 }
}