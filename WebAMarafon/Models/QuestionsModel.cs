using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAMarafon.Models
{
publicclassQuestions
 {
 [Key]
publicint ID { get; set; }
 [Required(ErrorMessage = "Question is Required !")]
publicstring Question { get; set; }
 [Required(ErrorMessage = "Course ID is Required !")]
publicstring CourseID { get; set; }
 }
}