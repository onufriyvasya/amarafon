using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAMarafon.Models
{
 public class Instructor
 {
 [Key]
 public int ID { get; set; }
 public string FirstName { get; set; }
 public string LastName { get; set; }
 public string Email { get; set; }
 public string password { get; set; }
 public string Repassword { get; set; }
 }
}
