using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAMarafon.Models
{
publicclassStudentCourseAssignment
 {
 [Key]
publicint ID { get; set; }
 [Required(ErrorMessage = "Instructor ID is Required")]
publicint StudentID { get; set; }
 [Required(ErrorMessage = "Course ID is Required")]
publicstring CourseID { get; set; }
 }
}