using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAMarafon.Models
{
publicclassResult
 {
 [Key]
publicint ID { get; set; }
 [Required(ErrorMessage = "Quiz ID is Required !")]
publicint QuizID { get; set; }
 [Required(ErrorMessage = "Student ID is Required !")]
publicint StudentID { get; set; }
 [Required(ErrorMessage = "Instructor ID is Required !")]
publicint InstructorID { get; set; }
 [Required(ErrorMessage = "Course ID is Required !")]
publicstring CourseID { get; set; }
 [Required(ErrorMessage = "Total Marks are Required !")]
publicint TotalMarks { get; set; }
 [Required(ErrorMessage = "Obtained Marks are Required !")]
publicint ObtainedMarks { get; set; }
 }
}