using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAMarafon.Models
{
publicclassQuiz
 {
 [Key]
publicint ID { get; set; }
 [Required(ErrorMessage = "Title is Required !")]
publicstring Title { get; set; }
 [Required(ErrorMessage = "Course ID is Required !")]
publicstring CourseID { get; set; }
 }
}
